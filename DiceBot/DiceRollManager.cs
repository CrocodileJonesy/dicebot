﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;


/// <summary>
/// Struct to store names of a modifier
/// </summary>
public struct Modifier
{
    /// <summary>
    /// Name of the modifier
    /// </summary>
    public string modifierName;

    /// <summary>
    /// Abbreviated name of the modifier
    /// </summary>
    public string modifierNameShort;
}

/// <summary>
/// Class to manage and store the dice and modifiers
/// </summary>
public class DiceRollManager
{
    /// <summary>
    /// Singleton instance for the dice roll manager
    /// </summary>
    private static DiceRollManager m_instance;


    ///// <summary>
    ///// List to store the dices that have been defined
    ///// </summary>
    //public Dictionary<string, int> diceList;

    /// <summary>
    /// List to store any modifers that have been defined
    /// </summary>
    public Dictionary<Modifier, int> modifiers;



    /// <summary>
    /// Single instance of the dice roll manager
    /// </summary>
    public static DiceRollManager Instance
    {
        get
        {
            // If an instance of this doesn't exist
            if (m_instance == null)
            {
                // Then create a new one
                m_instance = new DiceRollManager();
            }

            // Return instance
            return m_instance;
        }
    }

    /// <summary>
    /// Constructor to setup dice manager
    /// </summary>
    private DiceRollManager()
    {
        // Initialise dictionaires
        //diceList = new Dictionary<string, int>();
        modifiers = new Dictionary<Modifier, int>();
    }

    /// <summary>
    /// Loads the dice and modfiers data for the dice program
    /// </summary>
    public void LoadDiceData()
    {
        // Find and open the files
        //string[] diceFile = File.ReadAllLines(".\\DiceList.txt");
        string[] modFile = File.ReadAllLines(".\\ModifierList.txt");

        //// Go through dice file and load the specified dice data
        //for (int i = 0; i < diceFile.Length; ++i)
        //{
        //    // If the line is marked with a hash, then it is a comment to be ignored
        //    if (diceFile[i][0] == '#')
        //    {
        //        // Skip to next iteration
        //        continue;
        //    }

        //    // Split the strings
        //    string[] loadedStrings = diceFile[i].Split(',');

        //    // Add the parsed value to the dice list (Converted to lower case to avoid string problems)
        //    diceList.Add(loadedStrings[0].ToLower(), int.Parse(loadedStrings[1].ToLower()));
        //}

        // Go through the modifier data
        for (int i = 0; i < modFile.Length; ++i)
        {
            // If the line is marked with a hash, then it is a comment to be ignored
            if (modFile[i][0] == '#')
            {
                // Skip to nexy interation
                continue;
            }

            // Split the string data
            string[] loadedStrings = modFile[i].Split(',');

            // Setup a new modifier container
            Modifier mod = new Modifier();

            // Get the first entry data (That bieng modifier name)
            mod.modifierName = loadedStrings[0];

            // Get the abbreviated name of the modifier (If it exists)
            mod.modifierNameShort = (loadedStrings.Length == 3) ? loadedStrings[2] : string.Empty;

            // Add the processed modifier to the dictionary list 
            modifiers.Add(mod, int.Parse(loadedStrings[1]));
        }
    }

    /// <summary>
    /// Custom method based on TryParse to handle finding correct modifier
    /// </summary>
    /// <param name="modifier">The modifier to find</param>
    /// <param name="value">The found value (empty string if none found)</param>
    /// <returns></returns>
    public static bool TryGetModifier(string modifier, out string value)
    {
        // If the modifier string is valid
        if (!string.IsNullOrEmpty(modifier))
        {
            // Get the list of the modifiers
            Modifier[] mods = Instance.modifiers.Keys.ToArray();

            // Go through the modifier list
            for (int i = 0; i < mods.Length; ++i)
            {
                // Check to see if the input modifier exists and matches any modifier in the list
                if (modifier != null && (mods[i].modifierName.ToLower() == modifier.ToLower() || mods[i].modifierNameShort.ToLower() == modifier.ToLower()))
                {
                    // If found then assign to the value
                    value = modifier;

                    // Return that the modifier was found
                    return true;
                }
            }
        }

        // If this is reached, then no valid modifier was found
        // Return empty string
        value = string.Empty;

        // Return that modifier was not found
        return false;
    }
}