﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using System.Reflection;
using System.Diagnostics;
using System.ComponentModel;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

// TODO: Add commands to modify (add/remove) modifiers from the bot, rather than having to do through config file
// TODO: (Possibly) add random picker for modifiers if there is no modifier specified but option to apply mod with each dice roll has been specified
// TODO: Add support to have a possible number instead for modifiers

public class Program
{
    private DiscordSocketClient _client;
    private CommandService _commands;

    public static void Main(string[] args)
        => new Program().MainAsync().GetAwaiter().GetResult();

    public async Task MainAsync()
    {
        Console.Title = "Dice Bot v1.0";

        _client = new DiscordSocketClient();
        _commands = new CommandService();

        _client.Log += Log;
        await _client.LoginAsync(TokenType.Bot, LoadToken());
        await _client.StartAsync();

        _client.MessageUpdated += MessageUpdated;
        _client.Ready += () =>
        {
            Console.WriteLine("Bot is connected");
            return Task.CompletedTask;
        };

        await InstallCommandsAsync();

        DiceRollManager.Instance.LoadDiceData();

        await Task.Delay(-1);
    }

    private async Task MessageUpdated(Cacheable<IMessage, ulong> before, SocketMessage after, ISocketMessageChannel channel)
    {
        var message = await before.GetOrDownloadAsync();
        Console.WriteLine($"{message} -> {after}");
    }

    private Task Log(LogMessage msg)
    {
        Console.WriteLine(msg.ToString());
        return Task.CompletedTask;
    }

    public async Task InstallCommandsAsync()
    {
        // Hook the MessageReceived event into our command handler
        _client.MessageReceived += HandleCommandAsync;

        // Here we discover all of the command modules in the entry 
        // assembly and load them. Starting from Discord.NET 2.0, a
        // service provider is required to be passed into the
        // module registration method to inject the 
        // required dependencies.
        //
        // If you do not use Dependency Injection, pass null.
        // See Dependency Injection guide for more information.
        await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                        services: null);
    }

    private async Task HandleCommandAsync(SocketMessage messageParam)
    {
        // Don't process the command if it was a system message
        var message = messageParam as SocketUserMessage;
        if (message == null) return;

        // Create a number to track where the prefix ends and the command begins
        int argPos = 0;

        // Determine if the message is a command based on the prefix and make sure no bots trigger commands
        if (!(message.HasCharPrefix('!', ref argPos) ||
            message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
            message.Author.IsBot)
            return;

        // Create a WebSocket-based command context based on the message
        var context = new SocketCommandContext(_client, message);

        // Execute the command with the command context we just
        // created, along with the service provider for precondition checks.

        // Keep in mind that result does not indicate a return value
        // rather an object stating if the command executed successfully.
        var result = await _commands.ExecuteAsync(
            context: context,
            argPos: argPos,
            services: null);

        // Optionally, we may inform the user if the command fails
        // to be executed; however, this may not always be desired,
        // as it may clog up the request queue should a user spam a
        // command.
        if (!result.IsSuccess)
            await context.Channel.SendMessageAsync(result.ErrorReason);
    }

    /// <summary>
    /// Gets and returns bot token from file
    /// </summary>
    /// <returns></returns>
    public string LoadToken()
    {
        string line = string.Empty;
        string token = string.Empty;

        try
        {
            StreamReader configfile = new StreamReader(".\\config.ini");
            line = configfile.ReadLine();

            char[] splitLines = { '=' };

            while (line != null)
            {
                if (line.Contains("token"))
                {
                    string[] lines = line.Split(splitLines);

                    if (lines.Length == 2)
                    {
                        token = lines[1];
                        token.Replace(" ", string.Empty);
                    }

                    line = configfile.ReadLine();
                }
            }
        }
        catch (Exception e)
        {
            if (e.InnerException.GetType() == typeof(IOException))
            {
                Console.WriteLine("config.ini not found, program will now exit");
            }
            else
            {
                Console.WriteLine(string.Format("Error: {0}", e.Message));
            }
        }

        return token;
    }
}

//public class CommandHandler
//{
//    private readonly DiscordSocketClient _client;
//    private readonly CommandService _commands;

//    public CommandHandler(DiscordSocketClient client, CommandService commands)
//    {
//        _commands = commands;
//        _client = client;
//    }


//}