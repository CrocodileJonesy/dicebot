﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;

using Discord;
using Discord.Commands;


/// <summary>
/// Struct to store the data of the results of a roll
/// </summary>
public struct DiceRollResult
{
    /// <summary>
    /// The total amount from rolls
    /// </summary>
    public int result;

    /// <summary>
    /// the total amount of a modifier
    /// </summary>
    public int totalModifierNumber;

    /// <summary>
    /// The individual rolls from a dice roll
    /// </summary>
    public int[] rolls;

    /// <summary>
    /// The modifier (if any specified)
    /// </summary>
    public int modifierNumber;

    /// <summary>
    /// Modifer name
    /// </summary>
    public string modifierName;


    /// <summary>
    /// The final roll of dice
    /// </summary>
    public int FinalDiceResult
    {
        get
        {
            // Int to store value based on what contianer has
            int modNumber = -1;

            // If the dice has been rolled more than once
            if (rolls.Length > 1)
            {
                // Get the total modifier amount
                modNumber = totalModifierNumber;
            }
            else // If dice has been rolled just only once
            {
                // Get the single modifier value
                modNumber = modifierNumber;
            }

            // Return final dice value (Based on respectively found modifier value)
            return result + modNumber;
        }
    }
}

/// <summary>
/// Module handling the bot commands (And other things)
/// </summary>
public class DiceRollModule : ModuleBase<SocketCommandContext>
{
    /// <summary>
    /// Command to roll the dice (For "roll" command)
    /// </summary>
    /// <param name="par1">Param to input commands in any order</param>
    /// <param name="par2">Param to input commands in any order</param>
    /// <param name="par3">Param to input commands in any order</param>
    /// <param name="par4">Param to input commands in any order</param>
    /// <returns></returns>
    [Command("roll")]
    public Task RollDiceFlexibleAlt(string par1, string par2 = "", string par3 = "", string par4 = "")
    {
        // Simply call other roll dice since they do the same thing
        return RollDiceFlexible(par1, par2, par3, par4);
    }

    [Command("r"), Priority(3)]
    /// <summary>
    /// Command to roll the dice (For "r" (Shorthand for "roll") command)
    /// </summary>
    /// <param name="par1">Param to input commands in any order</param>
    /// <param name="par2">Param to input commands in any order</param>
    /// <param name="par3">Param to input commands in any order</param>
    /// <param name="par4">Param to input commands in any order</param>
    /// <returns></returns>
    public Task RollDiceFlexible(string par1, string par2 = "", string par3 = "", string par4 = "")
    {
        // The dice we will be using (Face on the die)
        string dice = string.Empty;

        // The number of times the dice will be rolled
        int numOfRolls = -1;

        // The modifier level (see modifers.txt for set values)
        string modifier = string.Empty;

        // Option to set whether modifiers are applied with each dice roll or after all die rolls
        bool sepModifiers = false;

        // Process all of the 4 parameters
        // This system allows for flexible handling of commands in any order (or certain input commands)
        ProcessCommand(par1, ref dice, ref numOfRolls, ref modifier, ref sepModifiers);
        ProcessCommand(par2, ref dice, ref numOfRolls, ref modifier, ref sepModifiers);
        ProcessCommand(par3, ref dice, ref numOfRolls, ref modifier, ref sepModifiers);
        ProcessCommand(par4, ref dice, ref numOfRolls, ref modifier, ref sepModifiers);

        // Setup string builder for any additional info
        StringBuilder additionalInfo = new StringBuilder();

        // If there is no dice specified
        if (string.IsNullOrEmpty(dice))
        {
            // Add to info that there is no dice and will be using a default instead
            additionalInfo.AppendLine("No dice specified; default dice \'d100\' used");
        }

        // If there is no dice rolls specified
        if (numOfRolls < 1)
        {
            // Then simply roll the dice once
            additionalInfo.AppendLine("No dice roll specified; default roll number of 1 used");
        }

        // If there is any modifier specified
        if (string.IsNullOrEmpty(modifier))
        {
            // If the option to apply modifier separately has been specified
            if (sepModifiers)
            {
                // Add to info that no modifier has been specified
                additionalInfo.AppendLine("No modifier to apply to dice rolls; modifier not applied");
            }
            else
            {
                // Add to info that no modifier specified
                additionalInfo.AppendLine("No modifier specified");
            }
        }

        // If there is no additional info to add
        if (string.IsNullOrEmpty(additionalInfo.ToString()))
        {
            // Add that there is none (To avoid crash for empty embed builder field)
            additionalInfo.Append("None");
        }

        // If no dice specified
        if (string.IsNullOrEmpty(dice))
        {
            // Use default d100 dice
            dice = "d100";
        }

        // If no number of rolls
        if (numOfRolls < 1)
        {
            // Roll dice just once
            numOfRolls = 1;
        }

        // Pass processed data to calculate
        return RollDiceCalc(dice, numOfRolls, modifier, sepModifiers, additionalInfo.ToString());
    }

    /// <summary>
    /// Reusable method to process string commands into usable data and vars
    /// </summary>
    /// <param name="command">The input command as string</param>
    /// <param name="dice">The found and returned dice</param>
    /// <param name="numOfRolls">The found and returned number of dice rolls</param>
    /// <param name="modifier">The found modifier value</param>
    /// <param name="individualModifiers">The found mod application value</param>
    private static void ProcessCommand(string command, ref string dice, ref int numOfRolls, ref string modifier, ref bool individualModifiers)
    {
        // Check if the passed command is valid (not empty)
        if (!string.IsNullOrEmpty(command))
        {
            // If the command has a d (For finding dice)
            if (command[0] == 'D' || command[0] == 'd')
            {
                // Check if the dice value hasn't already been assigned to
                if (string.IsNullOrEmpty(dice))
                {
                    // We have found our dice
                    dice = command;
                }
            }
            else if (numOfRolls < 1 && int.TryParse(command, out numOfRolls)) // If there already hasn't been a dice roll num AND there is a valid number
            {
                // roll number gets set in the TryParse command
            }
            else if (modifier == string.Empty && DiceRollManager.TryGetModifier(command, out modifier)) // If the modifier hasn't been found AND the modifier is valid (set in modifier.txt file)
            {
                // Modifier gets set in the TryGetModifier method
            }
            else if (command.ToLower() == "true"/* || command.ToLower() == "t")*/ && bool.TryParse(command, out individualModifiers)) // If command matches what we are looking AND parsing of value is valid
            {
                // Bool gets set in the TryParse method
            }
        }
    }

    /// <summary>
    /// Method that handles outputting of dice results to user
    /// </summary>
    /// <param name="dice">The X sided dice to use</param>
    /// <param name="rollTimes">The amount of times we will be rolling the dice (Default if 1)</param>
    /// <param name="modifier">The additional offset that will be used (Optional)</param>
    /// <param name="applyModIndividually">Option to apply modifiers to each dice roll or only one dice roll (Requires specified modifier)</param>
    /// <param name="additionalInfo">Any additional info to pass to the bot response to user</param>
    /// <returns></returns>
    public Task RollDiceCalc(string dice, int rollTimes = 1, string modifier = "", bool applyModIndividually = false, string additionalInfo = "None")
    {
        // Roll the dice and store results
        DiceRollResult diceResult = CalcDiceRoll(dice.ToLower(), rollTimes, modifier.ToLower(), applyModIndividually);

        // Prepare string builder for to organise the (mutiple) dice rolls
        StringBuilder rollString = new StringBuilder();

        // Bool to store if the modifier exists
        bool modifierExists = !string.IsNullOrEmpty(modifier);

        // Shorthand bool to store if we apply modifier to each dice roll
        bool applyModMulti = (modifierExists && applyModIndividually);

        // If there has been at least one dice rolled (if null then invalid data happened or something went wrong)
        if (diceResult.rolls != null)
        {
            // Add the first dice roll 
            rollString.Append(diceResult.rolls[0].ToString());

            // If we apply the modifier individually
            if (applyModMulti)
            {
                // Add string and data
                rollString.Append(string.Format(" + ({0})", diceResult.modifierNumber));
            }

            // If the dice has been rolled more than once
            if (diceResult.rolls.Length > 1)
            {
                // Add comma to separate dice
                rollString.Append(",");
            }

            // Go through extra rolled dice values (Exluding first and last dice rolls)
            for (int i = 1; i < (rollTimes - 1); ++i)
            {
                // Add dice roll to string
                rollString.AppendFormat(" {0}", diceResult.rolls[i].ToString());

                // If we apply the modifier individually to each dice roll
                if (applyModMulti)
                {
                    // Add the modifier to the dice roll
                    rollString.Append(string.Format(" + ({0})", diceResult.modifierNumber));
                }

                // Add comma for next dice roll
                rollString.Append(",");
            }

            // If the dice has been rolled more than once
            if (diceResult.rolls.Length > 1)
            {
                // Add last dice roll on the list to the string builder
                rollString.AppendFormat(" {0}", diceResult.rolls[diceResult.rolls.Length - 1].ToString());

                // If option to add modifiers individually specified
                if (applyModMulti)
                {
                    // Add modifier data
                    rollString.Append(string.Format(" + ({0})", diceResult.modifierNumber));
                }
            }
        }

        // Setup string for modifier
        string modString = string.Empty;

        // If option NOT specified to apply the modifier and only apply to final dice roll
        if (!applyModIndividually)
        {
            // If there has been a modifier found (value no 0)
            if (diceResult.modifierNumber > 0)
            {
                // Add data to string
                modString = string.Format("+ {0}", diceResult.modifierNumber);
            }
        }

        // Setup the embed builder (For bot output)
        EmbedBuilder embed = new EmbedBuilder();

        // Set title
        embed.WithTitle("Dice roll result:");

        // Add the final dice roll(s) number
        embed.AddField("Final number", diceResult.FinalDiceResult);

        // If there is a modifier
        if (diceResult.modifierNumber > 0)
        {
            // Add data for the modifier 
            embed.AddField("Modifier", string.Format("{0} ({1})", diceResult.modifierName, diceResult.modifierNumber));
        }

        // Setup string builder to use for details
        StringBuilder details = new StringBuilder();

        // Add the dice, amount of timex rolled, rolls and modifier value to the details field
        details.AppendFormat("{0}x{1} ({2}) {3}", dice.ToUpper(), rollTimes, rollString.ToString(), modString);

        // Add a new line
        details.AppendLine();

        // Add sum of rolled dice results
        details.AppendFormat("Total Dice Value: {0} ", diceResult.result);

        // If a modifier has been specified
        if (modifierExists)
        {
            // Add a new line
            details.AppendLine();

            // Add the sum of modifier value data
            details.AppendFormat("Total Modifier Value: {0}", diceResult.totalModifierNumber);
        }

        // Check if the string is within valid range
        if (details.Length <= 1024)
        {
            // Add the built details to the embed builder
            embed.AddField("Details", details.ToString());
        }
        else // String is larger than what is supported
        {
            // Reply with a message letting user know
            return ReplyAsync("Too large a number specified, please go for a lower roll number");
        }

        // Add entry for any additional info (Either none or will contain info based on user input)
        embed.AddField("Additional Info", additionalInfo);

        // If the specified dice doesn't exist
        if (diceResult.result == 0)
        {
            // Reply with invalid dice specified
            return ReplyAsync("Invalid dice specified");
        }
        else // Valid dice found
        {
            // Reply with built embed for nicer formatting
            return ReplyAsync("", false, embed.Build());
        }
    }

    //[Command("roll"), Priority(2)]
    //public Task RollDice(string dice, string modifier = "", int rollTimes = 1)
    //{
    //    return RollDiceCalc(dice, rollTimes, modifier);
    //}

    //[Command("r"), Priority(96)]
    //public Task RollDiceShort(string dice = "D100", int rollTimes = 1, string modifier = "")
    //{
    //    return RollDice(dice.ToLower(), rollTimes, modifier.ToLower());
    //}

    //[Command("r"), Priority(97)]
    //public Task RollDiceShort(string dice = "D100", string modifier = "", int rollTimes = 1)
    //{
    //    int oof = -1;
    //    if (int.TryParse(modifier, out oof))
    //    {
    //        return RollDice(dice, oof, string.Empty);
    //    }

    //    return RollDice(dice, rollTimes, modifier);
    //}

    //[Command("r"), Priority(98)]
    //public Task RollDiceAuto(int rollTimes = 1, string modifier = "")
    //{
    //    return RollDice("D100", rollTimes, modifier);
    //}

    //[Command("r"), Priority(99)]
    //public Task RollDiceAuto(string modifier = "", int rollTimes = 1)
    //{
    //    return RollDice("D100", rollTimes, modifier);
    //}

    /// <summary>
    /// Command to list usable commands
    /// </summary>
    /// <returns></returns>
    [Command("help")]
    public Task DiceHelp()
    {
        // Setup embed builder for bot response
        EmbedBuilder builder = new EmbedBuilder();

        // Add fields for each command that exists (plus descriptions)
        builder.WithTitle("Dice Roll bot");
        builder.AddField("!roll (X sided dice number)(dice rolls)(modifier)(apply modifier to individual dice roll)", "Rolls a dice with specified parameters, these parameters can be put in any order.\n" +
            "Parameters: (The maximum roll number of the dice roll) (The amount of times to roll the dice) (The modifier to use to offset the dice roll) (Apply the modifier to each indivdual dice roll or apply to the final number).\n" +
            "Example: !roll d6 2 l true.");
        builder.AddField("!r", "shorthand for \'roll\'");
        builder.AddField("!help", "Displays commands");
        builder.AddField("!modifier", "Lists all of the valid configured modifiers (from modifiers.txt)");
        builder.AddField("!m", "Same as \'modifier\'");

        // Reply with final result to user
        return ReplyAsync("", false, builder.Build());
    }

    /// <summary>
    /// Command that lists all of the configured modifiers
    /// </summary>
    /// <returns></returns>
    [Command("modifiers")]
    public Task ListModifiers()
    {
        // Setup embed builder for bot response
        EmbedBuilder builder = new EmbedBuilder();

        // Set title
        builder.WithTitle("Modifiers");

        // Get the list
        var list = DiceRollManager.Instance.modifiers;

        // Setup string builder to format data
        StringBuilder modString = new StringBuilder();

        // Go through data and add to the string builder list
        foreach (var mod in list)
        {
            // Add and format the modifier entry for each line
            modString.AppendLine(string.Format("{0} ({1}) = {2}", mod.Key.modifierName, mod.Key.modifierNameShort, mod.Value));
        }

        // Set the field description with the modifier list
        builder.WithDescription(modString.ToString());

        // Reply to user with final formatted data 
        return ReplyAsync("", false, builder.Build());
    }

    /// <summary>
    /// Shorthand command for "modifiers"
    /// </summary>
    /// <returns></returns>
    [Command("m")]
    public Task ListMods()
    {
        // Call list modifiers method
        return ListModifiers();
    }

    /// <summary>
    /// Calculates the dice roll(s) and returns final data
    /// </summary>
    /// <param name="diceNumber">The X sided dice to use</param>
    /// <param name="rollTimes">he amount of times the dice will be rolled</param>
    /// <param name="modifier">And value modifiers used</param>
    /// <param name="applyMod">Apply modifier value to each individual dice roll or to sum of dice rolls</param>
    /// <returns></returns>
    private DiceRollResult CalcDiceRoll(string diceNumber, int rollTimes, string modifier, bool applyMod)
    {
        // Setup dice roll number
        int diceRollNumber = -1;

        // Store whether a valid dice has been specified
        bool diceSpecified = (diceNumber[0] == 'd' || diceNumber[0] == 'D');

        // Setup new dice roll container
        DiceRollResult diceRollresult = new DiceRollResult();

        // If the dice was not found
        //if (!DiceRollManager.Instance.diceList.TryGetValue(diceNumber, out diceRollNumber))
        if (diceSpecified)
        {
            // Remove the letter to then get just the number
            string diceTemp = diceNumber.Remove(0, 1);

            // Parse the number as a int number to use
            diceRollNumber = int.Parse(diceTemp);
        }
        else // If there wasn't a dice specified
        {
            // Set the dice roll number (Which will be 0)
            diceRollresult.result = diceRollNumber; /*(applyMod) ? diceRollNumber * rollTimes : diceRollNumber;*/

            // Return the invalid/empty container (To then be picked up on and correct error handling run)
            return diceRollresult;
        }

        //diceRollNumber += DiceRollManager.Instance.diceList[number];

        // Setup var to store the found modifier data
        Modifier foundMod = new Modifier();

        // If there has been a modifier specified
        if (modifier != string.Empty)
        {
            // Get the list of modifiers
            Modifier[] mods = DiceRollManager.Instance.modifiers.Keys.ToArray();

            // Go through modifier list
            for (int i = 0; i < mods.Length; ++i)
            {
                // And check if the passed modifier is valid AND check to see if the passed modifier exists
                if (modifier != null && (mods[i].modifierName.ToLower() == modifier.ToLower() || mods[i].modifierNameShort.ToLower() == modifier.ToLower()))
                {
                    // If found then get the found modifier entry and store it
                    foundMod = mods[i];

                    // Store the name of the modifier
                    diceRollresult.modifierName = foundMod.modifierName;
                    break;
                }
            }
        }

        // Setup var to do the dice roll
        Random rand = new Random();

        // Setup the list of rolls to store the calculated number of each roll
        int[] rolls = new int[rollTimes];

        // Var to store the sum of all dice rolls
        int totalDiceResult = 0;

        // Var to store the modifier value
        int modifierNumber = 0;

        // Roll the dice as per the specified dice roll number
        for (int i = 0; i < rollTimes; ++i)
        {
            // Get the next random number based on the dice (1 being minimum and max being the X sided dice)
            rolls[i] = rand.Next(1, (diceRollNumber + 1));

            // Add the calculated number to the dice roll sum
            totalDiceResult += rolls[i];
        }

        // Check to see if the specified modifier exists (Just to make sure)
        if (modifier != string.Empty && DiceRollManager.Instance.modifiers.TryGetValue(foundMod, out modifierNumber))
        {
            // Get the value of the modifier
            modifierNumber = DiceRollManager.Instance.modifiers[foundMod];
            //totalDiceresult += (applyMod) ? modifierNumber * rollTimes : modifierNumber;
        }

        // Set the total modifier number based on whether we apply modifier to dice roll results as a sum or separately
        diceRollresult.totalModifierNumber = (applyMod) ? (modifierNumber * rollTimes) : modifierNumber;

        // Set the result of sum of the dice roll
        diceRollresult.result = totalDiceResult;

        // Add the data from the rolled dice
        diceRollresult.rolls = rolls;

        // Add the modifier value
        diceRollresult.modifierNumber = modifierNumber;

        // Return the calculated data
        return diceRollresult;
    }
}